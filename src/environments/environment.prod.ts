export const environment = {
  production: true,
  api: {
    host: 'recruits.siennsoft.com',
    protocol: 'http',
    path: 'api/'
  }
};
