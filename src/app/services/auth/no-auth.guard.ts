import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(private router: Router, private jwtHelperService: JwtHelperService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.jwtHelperService.tokenGetter() && !this.jwtHelperService.isTokenExpired()) {
      this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
