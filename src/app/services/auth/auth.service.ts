import { Injectable } from '@angular/core';
import { HttpRequestService } from '../http-request/http-request.service';
import { IJwtQuery, IJwtResponse } from '../../models/dto.model';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {
  static accessTokenKey = 'access_token';
  constructor(private httpRequest: HttpRequestService, private jwtHelper: JwtHelperService) {
  }

  login(loginData: IJwtQuery): Observable<any> {
    return this.httpRequest.post('Jwt', loginData, true)
      .pipe(map((result: IJwtResponse) => {
        localStorage.setItem(AuthService.accessTokenKey, result.access_token);
        return result;
      }));
  }

  logout(): Observable<any> {
    localStorage.removeItem(AuthService.accessTokenKey);
    return Observable.of(null);
  }
}
