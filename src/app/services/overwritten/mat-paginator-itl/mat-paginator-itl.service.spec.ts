import { TestBed, inject } from '@angular/core/testing';

import { MatPaginatorItlService } from './mat-paginator-itl.service';

describe('MatPaginatorItlOverrideService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MatPaginatorItlService]
    });
  });

  it('should be created', inject([MatPaginatorItlService], (service: MatPaginatorItlService) => {
    expect(service).toBeTruthy();
  }));
});
