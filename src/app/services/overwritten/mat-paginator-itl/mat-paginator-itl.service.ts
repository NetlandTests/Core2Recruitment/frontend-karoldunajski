import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class MatPaginatorItlService extends MatPaginatorIntl {

  private ofLabel = 'of';

  constructor(private translateService: TranslateService) {
    super();
    this.translateService.stream('paginator').subscribe((translations) => {
      this.firstPageLabel = translations.firstPageLabel;
      this.ofLabel = translations.ofLabel;
      this.itemsPerPageLabel = translations.itemsPerPageLabel;
      this.lastPageLabel = translations.lastPageLabel;
      this.nextPageLabel = translations.nextPageLabel;
      this.previousPageLabel = translations.previousPageLabel;
      this.changes.next();
    });
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 ${this.ofLabel} ${length}`;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex} ${this.ofLabel} ${length}`;
  }

}
