import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class HttpErrorHandlingInterceptor implements HttpInterceptor {

  private matSnackBar: MatSnackBar;

  constructor(private translateService: TranslateService, private injector: Injector) {
    if (this.matSnackBar === undefined) {
      this.matSnackBar = this.injector.get(MatSnackBar);
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .catch((error: HttpErrorResponse, caught) => {
        if (error instanceof HttpErrorResponse && (error.status === 0 || error.status > 499)) {
          this.translateService.get(['serverErrors.apiNotAvailable', 'close'])
            .subscribe((translation) => {
              this.matSnackBar.open(translation['serverErrors.apiNotAvailable'], translation['close']);
            });
        }
        return Observable.throw(error);
      });
  }
}
