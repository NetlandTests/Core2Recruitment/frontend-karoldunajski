import { Injectable } from '@angular/core';
import { HttpRequestService } from '../http-request/http-request.service';
import { Observer } from 'rxjs/Observer';
import { Product } from '../../models/product.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductsService {

  constructor(private httpRequestService: HttpRequestService) { }

  getAll(): Observable<Product[]> {
    return this.httpRequestService.get('Products');
  }

}
