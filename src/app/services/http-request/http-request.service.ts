import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class HttpRequestService {

  constructor(private httpClient: HttpClient) { }

  get(endpoint: string): Observable<any> {
    return this.httpClient.get(this.getFullEndpointUrl(endpoint));
  }

  post(endpoint: string, body, sendAsFormData = false): Observable<any> {
    let options;
    if (sendAsFormData) {
      options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };
      body = this.getHttpParamsBody(body);
    }
    return this.httpClient.post(this.getFullEndpointUrl(endpoint), body, options);
  }

  delete(endpoint: string): Observable<any> {
    return this.httpClient.delete(this.getFullEndpointUrl(endpoint));
  }

  put(endpoint: string, body): Observable<any> {
    return this.httpClient.put(this.getFullEndpointUrl(endpoint), body);
  }

  private getFullEndpointUrl(endpoint: string): string {
    return `${environment.api.protocol}://${environment.api.host}/${environment.api.path}${endpoint}`;
  }

  private getHttpParamsBody(body): HttpParams {
    let httpParamsBody = new HttpParams();
    _.forEach(body, (value, key) => {
      httpParamsBody = httpParamsBody.append(key, value);
    });
    return httpParamsBody;
  }

}
