import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppPath } from './app.path';
import { LoginComponent } from './components/login/login.component';
import { NoAuthGuard } from './services/auth/no-auth.guard';
import { AuthGuard } from './services/auth/auth.guard';
import { ProductsListComponent } from './components/products-list/products-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: `/${AppPath.PRODUCTS}`,
    pathMatch: 'full'
  },
  {
    path: AppPath.LOGIN,
    component: LoginComponent,
    canActivate: [NoAuthGuard]
  },
  {
    path: AppPath.PRODUCTS,
    component: ProductsListComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  providers: [],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }

export const appRoutingComponents = [
  LoginComponent,
  ProductsListComponent
];
