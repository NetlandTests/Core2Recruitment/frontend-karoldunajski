import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { JwtModule } from '@auth0/angular-jwt';
import { AppComponent } from './app.component';
import { AppSharedModule } from './app-shared.module';
import { environment } from '../environments/environment';
import { AuthService } from './services/auth/auth.service';
import { HttpRequestService } from './services/http-request/http-request.service';
import { AppRoutingModule, appRoutingComponents } from './app.routing.module';
import { AuthGuard } from './services/auth/auth.guard';
import { NoAuthGuard } from './services/auth/no-auth.guard';
import { ProductsService } from './services/products/products.service';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';
import { HttpErrorHandlingInterceptor } from './services/http-error-handling/http-error-handling.interceptor';

registerLocaleData(localePl, 'pl');

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function tokenGetter() {
  return localStorage.getItem(AuthService.accessTokenKey);
}

@NgModule({
  declarations: [
    AppComponent,
    appRoutingComponents
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    AppSharedModule,
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useFactory: createTranslateLoader, deps: [HttpClient] }
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [environment.api.host],
      }
    })
  ],
  providers: [
    HttpRequestService,
    AuthService,
    AuthGuard,
    NoAuthGuard,
    ProductsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorHandlingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
