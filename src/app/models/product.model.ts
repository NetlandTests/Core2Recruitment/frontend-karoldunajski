import { IProductDTO } from './dto.model';

export class Product implements IProductDTO {
  productID: number;
  name: string;
  price: number;
  description: string;
}
