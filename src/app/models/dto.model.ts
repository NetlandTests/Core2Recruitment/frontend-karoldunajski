export interface IJwtQuery {
  UserName: string;
  Password: string;
}

export interface IJwtResponse {
  access_token: string;
  expires_in: number;
}

export interface IProductDTO {
  productID: number;
  name: string;
  price: number;
  description: string;

}
