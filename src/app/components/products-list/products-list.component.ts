import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../../models/product.model';
import { ProductsService } from '../../services/products/products.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { AppPath } from '../../app.path';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  dataSource: MatTableDataSource<Product>;
  displayedColumns = ['productID', 'name', 'price', 'description'];
  products: Product[];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private productsService: ProductsService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.productsService.getAll().subscribe((products) => {
      this.dataSource =  new MatTableDataSource<Product>(products);
      this.dataSource.paginator = this.paginator;
    });
  }

  onSignoutClick($event) {
    this.authService.logout().subscribe(() => {
      this.router.navigate([AppPath.LOGIN]);
    });
  }

}
