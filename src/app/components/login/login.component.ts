import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { AppPath } from '../../app.path';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

enum FORM_CONTROL {
  NAME = 'name',
  PASSWORD = 'password'
}

enum SERVER_ERRORS {
  INVALID_CREDENTIALS = 'Invalid credentials'
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formControlNames = FORM_CONTROL;
  formGroup: FormGroup;

  serverError: string;

  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.formGroup = this.fb.group({
      [FORM_CONTROL.NAME]: new FormControl(null, [Validators.required]),
      [FORM_CONTROL.PASSWORD]: new FormControl(null, [Validators.required])
    });

    this.formGroup.valueChanges
      .subscribe((event) => {
        this.serverError = null;
      });
  }

  controlHasError(controlName: string, errorName: string): boolean {
    const control = this.formGroup.controls[controlName];
    return control && control.hasError(errorName);
  }

  onFromSubmit($event) {
    if (this.formGroup.valid) {
      this.authService.login({
        UserName: this.formGroup.value[FORM_CONTROL.NAME],
        Password: this.formGroup.value[FORM_CONTROL.PASSWORD]
      })
        .subscribe((result) => {
          this.router.navigate(['/']);
        }, (httpErrorResponse: HttpErrorResponse) => {
          this.serverError = httpErrorResponse.error;
        });
    }
  }

  getServerErrorLabel(): string {
    if (this.serverError === SERVER_ERRORS.INVALID_CREDENTIALS) {
      return 'serverErrors.invalidCredentials';
    }
    return 'serverErrors.unrecognizedError';
  }

  isSubmitButtonDisabled(): boolean {
    return this.serverError ? true : false || !this.formGroup.valid;
  }

}
