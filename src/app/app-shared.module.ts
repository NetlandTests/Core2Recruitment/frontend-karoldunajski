import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginatorItlService } from './services/overwritten/mat-paginator-itl/mat-paginator-itl.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    FlexLayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatToolbarModule,
    MatSnackBarModule
  ],
  declarations: [
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorItlService
    }],
  bootstrap: [],
  entryComponents: [
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    FlexLayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatToolbarModule,
    MatSnackBarModule
  ]
})

export class AppSharedModule { }
